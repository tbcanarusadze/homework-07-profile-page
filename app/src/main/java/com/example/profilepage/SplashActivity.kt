package com.example.profilepage

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.view.animation.AnimationUtils
import kotlinx.android.synthetic.main.activity_splash.*

class SplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        animation()
    }

    private val runnable = Runnable{
        openProfilePage()
    }

    override fun onStart(){
        super.onStart()
        Handler().postDelayed(runnable, 3000)
}
    override fun onPause(){
        super.onPause()
        Handler().removeCallbacks(runnable)
}

    private fun openProfilePage(){
        val intent = Intent(this,ProfilePageActivity::class.java )
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
        startActivity(intent)
    }

    private fun animation(){
        val animation = AnimationUtils.loadAnimation(this, R.anim.blink)
        text.startAnimation(animation)
    }

}
