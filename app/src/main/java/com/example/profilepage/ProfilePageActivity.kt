package com.example.profilepage

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_edit_profile.*
import kotlinx.android.synthetic.main.activity_profile_page.*

class ProfilePageActivity : AppCompatActivity() {

//    companion object{
//        const val REQUEST_CODE = 12
//    }

    private val requestCode = 10


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile_page)
        init()
    }

    private fun init(){
        editButton.setOnClickListener {
            editProfile()
        }
    }

    private fun editProfile(){
        val intent = Intent(this, EditProfileActivity::class.java)
        val userModel = UserModel(
            firstNameTextView.text.toString(),
            lastNameTextView.text.toString(),
            emailTextView.text.toString(),
            birthDateTextView.text.toString(),
            genderTextView.text.toString()
        )
        intent.putExtra("userModelInPut", userModel)
        startActivityForResult(intent, requestCode)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK && requestCode == requestCode) {
            val userModel = data?.extras?.getParcelable<UserModel>("userModelOutPut")
            firstNameTextView.text = userModel!!.firstName
            lastNameTextView.text = userModel.lastName
            emailTextView.text = userModel.email
            birthDateTextView.text = userModel.birthDate
            genderTextView.text = userModel.gender
        }

    }
}
