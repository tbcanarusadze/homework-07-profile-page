package com.example.profilepage

import android.app.Activity
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_edit_profile.*
import kotlinx.android.synthetic.main.activity_profile_page.*

class EditProfileActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_profile)
        init()
    }

    private fun init() {
        userInfo()
        saveButton.setOnClickListener {
            if (firstNameEditText.text.isEmpty() || lastNameEditText.text.isEmpty() || emailEditText.text.isEmpty() || birthDateEditText.text.isEmpty() || genderEditText.text.isEmpty()) {
                Toast.makeText(applicationContext, "Please fill all fields!", Toast.LENGTH_SHORT)
                    .show()
            } else {
                saveInfo()
            }
        }
    }

    private fun userInfo() {
        val userModel = intent.extras?.getParcelable<UserModel>("userModelInPut")
        firstNameEditText.setText(userModel!!.firstName)
        lastNameEditText.setText(userModel.lastName)
        emailEditText.setText(userModel.email)
        birthDateEditText.setText(userModel.birthDate)
        genderEditText.setText(userModel.gender)
    }

    private fun saveInfo() {
        val userModel = UserModel(
            firstNameEditText.text.toString(),
            lastNameEditText.text.toString(),
            emailEditText.text.toString(),
            birthDateEditText.text.toString(),
            genderEditText.text.toString()
        )
        intent.putExtra("userModelOutPut", userModel)
        setResult(Activity.RESULT_OK, intent)
        finish()
    }


}
